<?php
include "db.php";

$sql = "SELECT * FROM `tickets` WHERE tickets.data >= DATE_ADD(curdate(),INTERVAL -30 DAY) AND status IN(1,2,3) ORDER BY data ASC";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
     $data = array() ;
    while($row = $result->fetch_assoc()) {
        $data[] = $row;
    }
    echo json_encode($data);
} else {
    echo "0";
}
$conn->close();
?>
