import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './tickets/home/home.component';
import { AddComponent } from './tickets/add/add.component';
import { EditComponent } from './tickets/edit/edit.component';
import { ShowComponent } from './tickets/show/show.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {path:"", redirectTo:"/home", pathMatch:"full", canActivate: [AuthGuard]},
  {path:"home", component:HomeComponent, canActivate: [AuthGuard]},
  {path:"add", component:AddComponent, canActivate: [AuthGuard]},
  {path:"edit/:id", component:EditComponent, canActivate: [AuthGuard]},
  {path:"show/:id", component:ShowComponent, canActivate: [AuthGuard]},
  {path:"login", component:LoginComponent, },
  {path:"register", component:RegisterComponent},
  {path:"logout", component:LoginComponent}
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
