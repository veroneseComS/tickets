export class Ticket {
    nome: string;
    email: string;
    descricao: string;
    data: string;
    status: number;
}
