import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { TicketService } from '../../ticket.service';
import { Ticket } from '../../ticket';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

   constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ticketService: TicketService
  ) { }

  ngOnInit() {
      this.getSingleTicket();
  }

  //model:any={};
  model = new Ticket();
  id = this.route.snapshot.params['id'];
  getSingleTicket(){

    this.ticketService
      .getTicket(this.id)
      .subscribe(ticket =>{
          this.model = ticket[0];
          })
  };

  updateTicket(){
      this.ticketService
        .updateTicket(this.model)
        .subscribe(()=> this.goBack());
  }

   goBack(){
    this.router.navigate(['/home']);
  }
}
