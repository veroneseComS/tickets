import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../login/auth.service';
import { NavbarService } from '../../navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  mostrarMenu: boolean = false;
  currentUser : string;


    constructor(private authService: AuthenticationService,
                public nav: NavbarService){

    }

    ngOnInit(){
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(this.currentUser !='undefined') {
        this.mostrarMenu = true;
      }
      else{
        this.mostrarMenu = false;
      }
    }

}
