import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { TicketService } from '../../ticket.service';
import { Ticket } from '../../ticket';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ticketService: TicketService
  ) { }

  ngOnInit() {
    this.getSingleTicket();
  }
  ticket:Ticket;
  getSingleTicket(){
    var id = this.route.snapshot.params['id'];
    this.ticketService
      .getTicket(id)
      .subscribe(ticket =>{
          this.ticket = ticket[0];
          })
  };

  goBack(){
    this.router.navigate(['/home']);
  }
}
