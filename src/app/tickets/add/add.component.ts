import { Component, OnInit } from '@angular/core';
import { TicketService } from '../../ticket.service';
import { Ticket } from '../../ticket';
import {ActivatedRoute, Params, Router} from '@angular/router';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(
    private ticketService: TicketService,
     private router: Router) { }

  ngOnInit() {
  }

model = new Ticket();
  addTicket(){
      this.ticketService
        .addTicket(this.model)
        .subscribe(()=> this.goBack());
  }
   goBack(){
    this.router.navigate(['/home']);
  }
}
