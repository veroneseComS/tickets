import { NavbarService } from './../../navbar.service';
import { Component, OnInit } from '@angular/core';
import { TicketService } from '../../ticket.service';
import { Ticket } from '../../ticket';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { FilterPipe } from '../../filter.pipe';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _ticketService:TicketService,
    private router: Router,
    public nav: NavbarService
   ) { }

  tickets:any;
  ngOnInit() {
    this.getTickets();
    this.nav.show();
  }
  getTickets(){
     this._ticketService
        .getTickets()
        .subscribe(tickets => {
          this.tickets = tickets;
      } )
  }

  getUltimos15Dia(){
    this._ticketService
    .ultimos15Dia()
    .subscribe(tickets =>{
this.tickets = tickets;
    })
  }

  getUltimos30Dia(){
    this._ticketService
    .ultimos30Dia()
    .subscribe(tickets =>{
this.tickets = tickets;
    })
  }

  getUltimos60Dia(){
    this._ticketService
    .ultimos60Dia()
    .subscribe(tickets =>{
this.tickets = tickets;
    })
  }

  getLimpaTicketsFechados(){
    this._ticketService
      .limpaTicketsFechados()
      .subscribe(() => {
        this.getTickets();
    })
  }

  getTicketsLimpados(){
  this._ticketService
  .visualizarTicketsLimpados()
  .subscribe(tickets =>{
this.tickets = tickets;
  })
}

  deleteTickets(id){
      this._ticketService
        .deleteTicket(id)
        .subscribe(() => {
        this.getTickets();
      })
  }

}
