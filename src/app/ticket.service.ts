import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TicketService {
  tickets=[];
  constructor(private _http:Http) { }
  checkMe:any;

  getTickets(){
    return this._http.get("http://localhost/api/select.php/")
      .map(res=>{
        this.checkMe = res;

        if(this.checkMe._body !== "0"){
           return res.json()
        }

      });
  }

  addTicket(info){
    return this._http.post("http://localhost/api/insert.php",info)
      .map(()=>"");
  }

  getTicket(id){
    return this._http.post("http://localhost/api/selectone.php/",{'id':id})
      .map(res=>res.json());
  }

  deleteTicket(id){
    return this._http.post("http://localhost/api/delete.php/",{'id':id})
      .map(()=>this.getTickets());
  }

  updateTicket(info){
    return this._http.post("http://localhost/api/update.php/", info)
      .map(()=>"");
  }

  ultimos15Dia(){
    return this._http.get("http://localhost/api/selectultimos15.php")
      .map(res=>{
        this.checkMe = res;

        if(this.checkMe._body !== "0"){
          return res.json()
        }
      });
  }

  ultimos30Dia(){
    return this._http.get("http://localhost/api/selectultimos30.php")
      .map(res=>{
        this.checkMe = res;

        if(this.checkMe._body !== "0"){
          return res.json()
        }
      });
  }

  ultimos60Dia(){
    return this._http.get("http://localhost/api/selectultimos60.php")
      .map(res=>{
        this.checkMe = res;

        if(this.checkMe._body !== "0"){
          return res.json()
        }
      });
  }

  limpaTicketsFechados(){
    return this._http.get("http://localhost/api/limpaticketsfechados.php")
    .map(()=>"");
  }

  visualizarTicketsLimpados(){
    return this._http.get("http://localhost/api/visualizarticketslimpados.php")
    .map(res=>{
      this.checkMe = res;

      if(this.checkMe._body !== "0"){
        return res.json()
      }
    });
  }

}
