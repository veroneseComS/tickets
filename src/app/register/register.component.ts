import { AlertService } from './../alert.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent{
  model: any = {};
  loading = false;
  teste: string;

  constructor(private router: Router,
              private userService: UserService,
              private alertService: AlertService) { }


  register(){
    this.loading = true;

    this.userService.create(this.model)
      .subscribe(
        data => {
            console.log(data);
            this.alertService.success('Registro realizado com sucesso!', true);
        },
        error => {
          this.alertService.error('Não foi possivel realizar o cadastro, tente com um nome de usuário diferente');
          this.loading = false;
        });
  }

}
