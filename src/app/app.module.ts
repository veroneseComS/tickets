import { AlertComponent } from './diretivas/alert.component';
import { HttpClientModule} from '@angular/common/http';
import { AlertService } from './alert.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { NavbarComponent } from './tickets/navbar/navbar.component';
import { HomeComponent } from './tickets/home/home.component';
import { AddComponent } from './tickets/add/add.component';
import { EditComponent } from './tickets/edit/edit.component';
import { ShowComponent } from './tickets/show/show.component';
import { TicketService } from './ticket.service';
import { FilterPipe } from './filter.pipe';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserService } from './user.service';
import { AuthenticationService } from './login/auth.service';
import { AuthGuard } from './auth.guard';
import { NavbarService } from './navbar.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AddComponent,
    EditComponent,
    ShowComponent,
    FilterPipe,
    LoginComponent,
    RegisterComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [TicketService, AuthenticationService, UserService, AlertService, AuthGuard, NavbarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
