﻿import { contentHeaders } from './../common/headers';
import { Injectable } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Usuario } from './login/usuario';


@Injectable()
export class UserService {
    constructor(private http: HttpClient, private router: Router,  private route: ActivatedRoute) { }


    getAll() {
        return this.http.get<Usuario[]>('http://localhost/api/user.php');
    }

    getById(id: number) {
        return this.http.get('http://localhost/api/api.php' + id);
    }

    create(user: Usuario) {
        return this.http.post('http://localhost/api/api.php', JSON.stringify(user));
    }


    update(user: Usuario) {
        return this.http.put('http://localhost/api/api.php' + user.id, user);
    }

}
