import { NavbarService } from './../navbar.service';
import { Usuario } from './usuario';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../alert.service';
import { AuthenticationService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

model: any = {};
loading = false;
returnUrl: string;

private usuario: Usuario = new Usuario();

  constructor(private authService: AuthenticationService,
              private route: ActivatedRoute,
              private router: Router,
              private alertService: AlertService,
              public nav: NavbarService
            ) { }

  ngOnInit() {
    this.authService.logout();
    this.nav.hide();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login(){
    this.loading = true;
    this.authService.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          if(data.status==200){
            this.router.navigate(['/home']);
          }else{
            this.alertService.error('Usuário ou senha inválidos');
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}
