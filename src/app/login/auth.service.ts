﻿import { Injectable, EventEmitter } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map'
import { contentHeaders } from '../../common/headers';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient,private router: Router,  private route: ActivatedRoute) { }

    usuarioAutenticado: boolean = false;


    login(username: string, password: string) {
        return this.http.post<any>('http://localhost/api/login.php', JSON.stringify({ username: username, password: password }))
            .map(user => {
                //realiza login se a jwt token tiver correta

                if (user.status==200) {
                  this.usuarioAutenticado = true;
                    //guarda os detalhes do usuário e o jwt em um local storage para manter o usuario logado independente de refreshs
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                else{
                  this.usuarioAutenticado = false;
                }

                return user;
            });
    }

    logout() {
        //remove o usuário do local storage
        localStorage.removeItem('currentUser');
    }
}
