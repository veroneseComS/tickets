import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(tickets: any, termo: any): any {
    //checa se o termo da pesquisa é indefinido
    if(termo === undefined) return tickets;
    //retorna o array de tickets atualizado
    return tickets.filter(function(ticket){
      return ticket.nome.toLowerCase().includes(termo.toLowerCase());
    })

  }

}
