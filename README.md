# Sistema de tickets realizado para Sikoni

## Ferramentas Utilizadas no Frontend:

Angular 2+;
Html/Css;
Bootstrap

## Ferramentas Utilizadas no Backend:

Php;

## Banco de dados:

Mysql;

Os arquivos do banco de dados estão nomeados de mydb.sql

## Como iniciar o sistema:
1. Navegue até a pasta, copie a pasta api para C:/xamp/htdocs
2. Inicie o node, navegue até a pasta e execute npm install para baixar as dependências necessárias
3. Inicie o servidor apache/mysql no xamp;
4. Execute ng serve no node.